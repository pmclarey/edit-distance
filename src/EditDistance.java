public class EditDistance {
    private static final int MATCH = 0;
    private static final int INSERT = 1;
    private static final int DELETE = 2;
    private static Cell[][] m;
    private static String s;
    private static String t;
    private static StringBuilder traceback = new StringBuilder();
    
    public static void main(String[] args) {
        if (args.length < 2) {
            System.err.println(
                    "Usage: java EditDistance <String 1> <String 2>");
            System.exit(1);
        }
        
        s = " " + args[0];
        t = " " + args[1];
        
        int editDistance = stringCompare();
        reconstructPath(s.length() - 1, t.length() - 1);
        System.out.println("Edit distance: " + editDistance);
        System.out.println("Path: " + traceback);
        StringBuilder morphString = new StringBuilder(args[0]);
        System.out.println(morphString);
        for (int i = 0, j = 0, k = 1; i < traceback.length(); i++) {
            switch(traceback.charAt(i)) {
                case 'M':
                    j++;
                    k++;
                    break;
                case 'S':
                    morphString.setCharAt(j++, t.charAt(k++));
                    System.out.println(morphString);
                    break;
                case 'I':
                    morphString.insert(j++, t.charAt(k++));
                    System.out.println(morphString);
                    break;
                case 'D':
                    morphString.deleteCharAt(j);
                    System.out.println(morphString);
                    break;
            }
        }
    }
    
    private static int stringCompare() {
        int dim = Math.max(s.length(), t.length());
        m = new Cell[dim][dim];
        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < dim; j++) {
                m[i][j] = new Cell();
            }
        }
        
        for (int i = 0; i < dim; i++) {
            row_init(i);
            col_init(i);
        }
        
        int[] opt = new int[3];
        
        for (int i = 1; i < s.length(); i++) {
            for (int j = 1; j < t.length(); j++) {
                opt[MATCH] = m[i - 1][j - 1].cost
                        + match(s.charAt(i), t.charAt(j));
                opt[INSERT] = m[i][j - 1].cost + 1;
                opt[DELETE] = m[i - 1][j].cost + 1;
                
                m[i][j].cost = opt[MATCH];
                m[i][j].parent = MATCH;
                for (int k = INSERT; k <= DELETE; k++) {
                    if (opt[k] < m[i][j].cost) {
                        m[i][j].cost = opt[k];
                        m[i][j].parent = k;
                    }
                }
            }
        }
        
        int i = s.length() - 1;
        int j = t.length() - 1;
        
        return m[i][j].cost;
    }
    
    private static void reconstructPath(int i, int j) {
        switch (m[i][j].parent) {
            case -1:
                break;
            case MATCH:
                reconstructPath(i - 1, j - 1);
                traceback.append((s.charAt(i) == t.charAt(j)) ? "M" : "S");
                break;
            case INSERT:
                reconstructPath(i, j - 1);
                traceback.append("I");
                break;
            case DELETE:
                reconstructPath(i - 1, j);
                traceback.append("D");
                break; 
        }
    }
    
    private static void row_init(int i) {
        m[0][i].cost = i;
        if (i > 0) {
            m[0][i].parent = INSERT;
        } else {
            m[0][i].parent = -1;
        }
    }
    
    private static void col_init(int i) {
        m[i][0].cost = i;
        if (i > 0) {
            m[i][0].parent = DELETE;
        } else {
            m[i][0].parent = -1;
        }
    }
    
    private static int match(char c, char d) {
        return (c == d) ? 0 : 1;
    } 
}

class Cell {
    int cost;
    int parent;
}
